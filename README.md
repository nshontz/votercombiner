# Voter Combiner.

## Installation

    Add to your composer.json:
        "repositories": [
                {
                    "type": "git",
                    "url": "https://nshontz@bitbucket.org/nshontz/voter_combiner.git"
                }
            ],
            "require": {
                "nshontz/voter_combiner": "dev-master"
            }

## Usage
