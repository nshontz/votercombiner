<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nick
 * Date: 10/18/13
 * Time: 10:32 AM
 * To change this template use File | Settings | File Templates.
 */

namespace NShontz\VoterCombiner;


class Address
{
    var $name, $send_to, $city, $state, $zip;

    function __construct($name, $send_to, $city, $state, $zip)
    {
        $this->name = $name;
        $this->send_to = $send_to;
        $this->city = $city;
        $this->state = $state;
        $this->zip = $zip;
    }
}