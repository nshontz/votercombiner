<?php

namespace NShontz\VoterCombiner;

class VoterCombiner
{
    function __construct()
    {
    }

    private function get_unique_send_to($voters)
    {

        $addresses = array();
        foreach ($voters as $voter) {
            $key = md5(trim($voter->send_to));
            if (!isset($addresses[$key])) {
                $addresses[$key] = array();
            }
            $addresses[$key][] = $voter;
        }

        return $addresses;
    }

    private function format_name($data)
    {
        return ucwords(strtolower(trim($data)));
    }

    public function get_unique_voting_addresses()
    {

        $voters = Data::get_voters();
        $addresses = $this->get_unique_send_to($voters);
        $data = array();
        foreach ($addresses as $address) {
            if (count($address) == 1) {
                $name = $this->format_name($address[0]->first) . " " . $this->format_name($address[0]->last);
            } else {
                $name = "";
                $last_names = array();
                foreach ($address as $voter) {
                    if (!in_array($voter->last, $last_names)) {
                        $last_names[] = $voter->last;
                    }
                }

                switch (count($last_names)) {
                    case 1:
                        if (count($address) == 3) {
                            $name = "The " . $this->format_name($last_names[0]) . " Family";
                        } else if ($address[0]->last == $address[1]->last) {
                            $name = $this->format_name($address[0]->first) . " and " . $this->format_name($address[1]->first) . " " . $this->format_name($address[1]->last);
                        }
                        break;
                    case 2:
                        if (count($address) == 3) {
                            $name = "The " . $this->format_name($last_names[0]) . " and " . $this->format_name($last_names[1]) . " Household";
                        } else {
                            $name = $this->format_name($address[0]->first) . " " . $this->format_name($address[0]->last) . " and " . $this->format_name($address[1]->first) . " " . $this->format_name($address[1]->last);
                        }
                        break;
                    default:
                        $name = "Our Friends at " . $this->format_name($address[0]->send_to);
                        break;
                }
            }

            $data[] = new address($name,
                $this->format_name($address[0]->send_to),
                $this->format_name($address[0]->city),
                $address[0]->state,
                $address[0]->zip);
        }
        Data::update_unique_voters($data);

        return $data;
    }
}