<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nick
 * Date: 10/18/13
 * Time: 10:34 AM
 * To change this template use File | Settings | File Templates.
 */

namespace NShontz\VoterCombiner;


class Data
{
    static function get_voters()
    {
        $function = "get_voters_from_" . \Config::$datasource;
        return Data::$function();
    }

    static function get_initial_voter_count()
    {
        $function = "get_voter_count_from_" . \Config::$datasource;
        return Data::$function();
    }

    static function update_unique_voters($voters)
    {
        $function = "update_unique_voters_" . \Config::$datasource;
        return Data::$function($voters);
    }

    static function get_voter_count_from_csv()
    {
        return count(Data::get_voters_from_csv());
    }

    static function get_voter_count_from_mysql()
    {
        return count(Data::get_voters_from_mysql());
    }

    static function get_voters_from_csv()
    {
        $filename = 'data/data.csv';

        $voters = array();
        if (($handle = fopen($filename, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $voters[$data['0']] = new voter($data['0'], $data['1'], $data['2'], $data['3'], $data['4'], $data['5'], $data['6'], $data['7']);

            }
            fclose($handle);
        }
        if (\Config::csv_has_headings) {
            array_shift($voters);
        }
        asort($voters);

        return $voters;
    }

    static function db_check()
    {
        $response = true;

        $username = \Config::username;
        $password = \Config::password;
        $hostname = \Config::hostname;
        $database = \Config::database;
        $tablename = \Config::tablename;

        //connection to the database
        $dbhandle = mysql_connect($hostname, $username, $password)
        or $response = false;

        //select a database to work with
        $selected = mysql_select_db($database, $dbhandle)
        or $response = false;

        //execute the SQL query and return records

        $sql = "SELECT * FROM " . $database . "." . $tablename . " order by voterid LIMIT 1";
        $result = mysql_query($sql);
        //fetch tha data from the database

        if (!$result) {
            $response = false;
        }

        return $response;

    }

    static function get_voters_from_mysql()
    {
        $username = \Config::username;
        $password = \Config::password;
        $hostname = \Config::hostname;
        $database = \Config::database;
        $tablename = \Config::tablename;

        //connection to the database
        $dbhandle = mysql_connect($hostname, $username, $password)
        or die("Unable to connect to MySQL");

        //select a database to work with
        $selected = mysql_select_db($database, $dbhandle)
        or die("Could not select database" . $database);

        //execute the SQL query and return records

        $sql = "SELECT * FROM " . $database . "." . $tablename . " order by voterid";
        $result = mysql_query($sql);
        //fetch tha data from the database

        if ($result) {

            $voters = array();
            while ($row = mysql_fetch_array($result)) {
                $voters[] = new voter($row['voterid'], $row['last'], $row['first'], $row['address'], $row['send_to'], $row['city'], $row['state'], $row['zip']);
            }
        } else {
            die($sql . "<br/>" . mysql_error($dbhandle));
        }

        //close the connection
        mysql_close($dbhandle);

        return $voters;
    }

    static function update_unique_voters_csv($voters)
    {

        $filename = 'data/unique_data.csv';
        unlink($filename);
        $fp = fopen($filename, 'w');

        foreach ($voters as $fields) {
            fputcsv($fp, (array)$fields);
        }

        fclose($fp);
    }

    static function update_unique_voters_mysql($voters)
    {
        Data::truncate_unique_voters();
        foreach ($voters as $voter) {
            Data::add_voter($voter->name, $voter->send_to, $voter->city, $voter->state, $voter->zip);
        }

    }

    static function truncate_unique_voters()
    {
        $username = \Config::username;
        $password = \Config::password;
        $hostname = \Config::hostname;
        $database = \Config::database;

        //connection to the database
        $dbhandle = mysql_connect($hostname, $username, $password)
        or die("Unable to connect to MySQL");

        //select a database to work with
        $selected = mysql_select_db($database, $dbhandle)
        or die("Could not select database " . $database);

        $sql = "TRUNCATE unique;";
        mysql_query($sql);

        //close the connection
        mysql_close($dbhandle);
    }

    static function add_voter($name, $send_to, $city, $state, $zip)
    {
        $username = \Config::username;
        $password = \Config::password;
        $hostname = \Config::hostname;
        $database = \Config::database;

        //connection to the database
        $dbhandle = mysql_connect($hostname, $username, $password)
        or die("Unable to connect to MySQL");

        //select a database to work with
        $selected = mysql_select_db($database, $dbhandle)
        or die("Could not select database " . $database);

        $sql = "INSERT INTO unique VALUES (null, '" . $name . "', '" . $send_to . "','" . $city . "','" . $state . "','" . $zip . "')";
        mysql_query($sql);

        //close the connection
        mysql_close($dbhandle);
    }

}