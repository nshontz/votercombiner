<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nick
 * Date: 10/18/13
 * Time: 10:32 AM
 * To change this template use File | Settings | File Templates.
 */

namespace NShontz\VoterCombiner;

class Voter {
    var $id, $last, $first, $address, $send_to, $city, $state, $zip;

    function __construct($id, $last, $first, $address, $send_to, $city, $state, $zip)
    {
        $this->id = $id;
        $this->last = $last;
        $this->first = $first;
        $this->address = $address;
        $this->send_to = $send_to;
        $this->city = $city;
        $this->state = $state;
        $this->zip = $zip;
    }

}